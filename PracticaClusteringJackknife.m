%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% D e t e c c i � n   d e   o b s e r v a c i o n e s  
% i n f l u y e  n t e s :  M � t o d o  d e  J A C K N I  F E
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc,clear,close all;
load('wholesalecustomers.mat')  % carga de datos del ejemplo
N=size(Wholesalecustomersdata,1);     % n�mero de observaciones
m=2;
MaxIteraciones=100; % n�mero de iteraciones
Tolerancia= 1e-5;   % tolerancia en el criterio de parada
Visualizacion=0;    % 0/1
opciones=[m,MaxIteraciones,Visualizacion];
K=3;             % n�mero de clusters
for i=1:N-1
    Wholesalecustomersdata_sin_i=[Wholesalecustomersdata(1:(i-1),:);Wholesalecustomersdata((i+1):N,:)]; % eliminacion de la observaci�n i
    [center,U,obj_fcn] = fcm(Wholesalecustomersdata_sin_i, K,opciones);
    SSE(i)=sum(obj_fcn); % La suma del resultado del algoritmo nos da el valor SSE(i) para obtener los outliers   
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% D e t e c c i � n   v i s u a l
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
plot(SSE)
xlabel('Dato i','fontsize',18)
ylabel('SSE eliminando el dato i','fontsize',18)
grid on
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% D e t e c c i � n   a n a l � t i c a 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sigma=std(SSE);    % desviaci�n t�pica
mu=mean(SSE);      % media 
umbral=1.98;       % umbral =2 para distribuciones normales
                   % umbral =3 para cualquier ditribuci�n
outliers=[];       % inicializaci�n del vector de outliers
for i=1:N-1
    if abs(SSE(i)-mu)>umbral*sigma
        outliers=[outliers,i];
    end
end
outliers            % impresi�n por pantalla de los outliers 

% se�alamos los outliers en la gr�fica
hold on
for i=1:length(outliers)
    dato=outliers(i);
    plot(dato,SSE(dato),'bo',...
    'MarkerSize',6,'MarkerEdgeColor','b', 'MarkerFaceColor','b')
    text(dato,SSE(dato),'Outlier','fontsize',18);
end