%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% calculo del numero de cluster K
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all, clear clc   % cerrrar  ventanas gr�ficas 
                        % borrar memoria y  consola
load('wholesalecustomers.mat')  % carga de datos Whole Sale Customers 
Kmax=10;
m=2;                       % par�metro de fcm, 2 es el defecto
MaxIteraciones=100;        % n�mero de iteraciones
Tolerancia= 1e-05;          % tolerancia en el criterio de para
Visualizacion=0;           % 0/1
opciones=[m,MaxIteraciones,Visualizacion];

for K=2:Kmax
    
    [center,U,obj_fcn] = fcm(Wholesalecustomersdata, K,opciones); %Aplicamos el algoritmo fuzzy c means
    for i=1:K
    maxU=max(U); % calculo del m�ximo nivel de pertenencia de los
             % individuos
    individuos=find(U(i,:)==maxU);% calcula los individuos del
                              % grupo i que alcanzan el m�ximo
    cidx(individuos)=i;           % asigna estos individuos al grupo i
    grado_pertenecia(individuos)=maxU(individuos);
    end
    [Bic_K,xi]=BIC(K,cidx,Wholesalecustomersdata); %Utilizamos cidx para el c�lculo de BIC
    BICK(K)=Bic_K;
end
figure(2)
%Representaci�n visual de los grupos
plot(2:K',BICK(2:K)','s-','MarkerSize',6,...
     'MarkerEdgeColor','r', 'MarkerFaceColor','r')
xlabel('K','fontsize',18)      % etiquetado del eje-x
ylabel('BIC(K)','fontsize',18) % etiquetado del eje-y
print -depsc2 -tiff 'PracticaClusteringDeteccionK'