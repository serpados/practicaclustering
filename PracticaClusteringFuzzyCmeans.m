%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ejemplo   a l g o r i t m o   d e   l a s  
%            F u z z y   c - m e d i a s
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all, clear clc   % cerrrar  ventanas gr�ficas 
                        % borrar memoria y  consola
load('wholesalecustomers.mat')  % carga de datos Whole Sale Customers 
K=5;                       % n�mero de clusters
m=2;                       % par�metro de fcm, 2 es el defecto
MaxIteraciones=200;        % n�mero de iteraciones
Tolerancia= 1e-5;          % tolerancia en el criterio de para
Visualizacion=0;           % 0/1
opciones=[m,MaxIteraciones,Visualizacion];
[center,U,obj_fcn] = fcm(Wholesalecustomersdata, K,opciones);
%%%%%%%
% p a r � m e t r o s   d e   s a l i d a              
% center    centroides de los grupos
% U         matriz de pertenencia individuo cluster 
% obj_fun   funci�n objetivo
%%%%%%  Asignaci�n de individuo a grupo, maximizando el nivel de
%       pertenencia al grupo
for i=1:K
maxU=max(U); % calculo del m�ximo nivel de pertenencia de los
             % individuos
individuos=find(U(i,:)==maxU);% calcula los individuos del
                              % grupo i que alcanzan el m�ximo
cidx(individuos)=i;           % asigna estos individuos al grupo i
grado_pertenecia(individuos)=maxU(individuos);
end
hold on
%variables de salida
plot(Wholesalecustomersdata(cidx==1,1),Wholesalecustomersdata(cidx==1,2),Wholesalecustomersdata(cidx==1,3),Wholesalecustomersdata(cidx==1,4),Wholesalecustomersdata(cidx==1,5),Wholesalecustomersdata(cidx==1,6),'rs','MarkerSize',6,'MarkerEdgeColor','r','MarkerFaceColor','r');
%centroides de salida
plot(center(1,1),center(1,2),center(1,3),center(1,4),center(1,5),center(1,6),'xk','MarkerSize',10,'LineWidth',3);
hold on
plot(Wholesalecustomersdata(cidx==2,1),Wholesalecustomersdata(cidx==2,2),Wholesalecustomersdata(cidx==2,3),Wholesalecustomersdata(cidx==2,4),Wholesalecustomersdata(cidx==2,5),Wholesalecustomersdata(cidx==2,6),'rs','MarkerSize',6,'MarkerEdgeColor','b', 'MarkerFaceColor','b');
plot(center(2,1),center(2,2),center(2,3),center(2,4),center(2,5),center(2,6),'xk','MarkerSize',10,'LineWidth',3);
hold on
plot(Wholesalecustomersdata(cidx==3,1),Wholesalecustomersdata(cidx==3,2),Wholesalecustomersdata(cidx==3,3),Wholesalecustomersdata(cidx==3,4),Wholesalecustomersdata(cidx==3,5),Wholesalecustomersdata(cidx==3,6),'rs','MarkerSize',6,'MarkerEdgeColor','g', 'MarkerFaceColor','g');
plot(center(3,1),center(3,2),center(3,3),center(3,4),center(3,5),center(3,6),'xk','MarkerSize',10,'LineWidth',3);
hold on
plot(Wholesalecustomersdata(cidx==4,1),Wholesalecustomersdata(cidx==4,2),Wholesalecustomersdata(cidx==4,3),Wholesalecustomersdata(cidx==4,4),Wholesalecustomersdata(cidx==4,5),Wholesalecustomersdata(cidx==4,6),'rs','MarkerSize',6,'MarkerEdgeColor','y', 'MarkerFaceColor','y');
plot(center(4,1),center(4,2),center(4,3),center(4,4),center(4,5),center(4,6),'xk','MarkerSize',10,'LineWidth',3);
hold on
plot(Wholesalecustomersdata(cidx==5,1),Wholesalecustomersdata(cidx==5,2),Wholesalecustomersdata(cidx==5,3),Wholesalecustomersdata(cidx==5,4),Wholesalecustomersdata(cidx==5,5),Wholesalecustomersdata(cidx==5,6),'rs','MarkerSize',6,'MarkerEdgeColor','c', 'MarkerFaceColor','c');
plot(center(5,1),center(5,2),center(5,3),center(5,4),center(5,5),center(5,6),'xk','MarkerSize',10,'LineWidth',3);
%Representaci�n visual 
xlabel('x_1','fontsize',18),ylabel('x_2','fontsize',18)
legend('Grupo 1','Grupo 2','Grupo 3','Grupo 4','Grupo 5'),axis('square'), box on
title('Algoritmo Fuzzy c-means','fontsize',18)
print(1,'-depsc','resul_fcm')   % genera gr�fico .eps en fichero   

